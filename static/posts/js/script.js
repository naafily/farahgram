$("#like").click(function () {

    console.log(12 + 3);

    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    const csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var post_id = $('#like').attr('data-id')
    var like = $('#like').text()


    if (like == 'like') {
        var url = '/post/like/'
        var btn_text = 'unlike'
        var btn_class = 'btn btn-warning'
    }
    else {
        var url = '/post/unlike/'
        var btn_text = 'like'
        var btn_class = 'btn btn-dark'

    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {
            'post_id': post_id,
        },
        success: function (data) {
            if (data['status'] == 'ok') {
                $('#like').text((btn_text))
                $('#like').attr({'class': btn_class})

            }

        }
    });
});

