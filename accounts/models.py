from django.db import models

from django.contrib.auth.models import User


class Profile(models.Model):
    age = models.PositiveIntegerField(null=True, blank=True)
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    friend = models.ManyToManyField('self', symmetrical=False, through='Relationship')


class Relationship(models.Model):
    followed_by = models.ForeignKey(Profile, related_name="follows_set", on_delete=models.CASCADE)
    follows = models.ForeignKey(Profile, related_name="followed_by_set", on_delete=models.CASCADE)
