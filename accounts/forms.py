from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .models import Profile


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    age = forms.IntegerField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',
                  'age')


class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['age', ]

    def save(self, commit=True):
        profile = super().save()
        # profile.user.email = self.cleaned_data['email']
        profile.user.save()
        return profile


class UpdateUserForm(forms.ModelForm):
    username = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)

        def __init__(self, *args, **kwargs):
            self.username = kwargs.pop('username')
            super().__init__(*args, **kwargs)

        def clean_email(self):
            email = self.cleaned_data['email']
            if self.username is not None and email and User.objects.filter(email=email).exclude(
                    username=self.username).count():
                raise forms.ValidationError(
                    'This email address is already in use. Please supply a different email address.')
            return email
