from django.views.generic import CreateView, DetailView, UpdateView, ListView, View
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.contrib.messages.views import SuccessMessageMixin

from .forms import SignUpForm, UpdateUserForm, UpdateProfileForm
from .models import Profile, Relationship

from posts.models import Post


class SignUpView(SuccessMessageMixin, CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
    success_message = " your account was created successfully"

    def form_valid(self, form):
        obj = form.save()
        profile = Profile(age=form.cleaned_data['age'],
                          user=obj)
        profile.save()
        return HttpResponseRedirect(reverse('login'))


class DashboardView(ListView):
    model = Post
    template_name = 'dashboard.html'

    def get_queryset(self):
        return Post.objects.filter(author__user_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_profile = Profile.objects.get(user_id=self.kwargs['pk'])
        context['user_profile'] = user_profile
        is_following = False
        rel = Relationship.objects.filter(follows_id=user_profile.id, followed_by_id=self.request.user.profile.id)
        if rel.exists():
            is_following = True

        context['is_following'] = is_following
        return context


class ProfileView(DetailView):
    model = Profile
    template_name = 'profile_detail.html'

    def get_object(self, queryset=None):
        return Profile.objects.get(user_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_profile = Profile.objects.get(user_id=self.kwargs['pk'])
        context['user_profile'] = user_profile
        is_following = False
        rel = Relationship.objects.filter(follows_id=user_profile.id, followed_by_id=self.request.user.profile.id)
        if rel.exists():
            is_following = True

        context['is_following'] = is_following
        return context


class UpdateProfileView(UpdateView):
    model = User
    form_class = UpdateUserForm
    second_form_class = UpdateProfileForm
    template_name = "profile_update.html"

    def get(self, request):
        self.object = self.get_object()
        form = self.form_class(instance=request.user)
        form2 = self.second_form_class(instance=request.user.profile)
        return self.render_to_response(self.get_context_data(
            object=self.object, form=form, form2=form2))

    def post(self, request):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=request.user)
        form2 = self.second_form_class(request.POST, instance=request.user.profile)

        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(form=form, form2=form2, username=request.user.username))

    def get_object(self):
        return User.objects.get(username=self.request.user.username)

    def get_success_url(self):
        return reverse('profile', kwargs={'pk': self.request.user.pk})


class MyLoginView(SuccessMessageMixin, LoginView):
    success_message = " you have logged in successfully"

    def get_success_url(self):
        url = self.get_redirect_url()
        return url or reverse('profile', kwargs={'pk': self.request.user.pk})


class Follow(View):
    def post(self, request):
        profile = Profile.objects.get(id=request.user.profile.id)
        profile_to_follow = Profile.objects.get(id=request.POST['user_id'])
        if profile == profile_to_follow:
            raise Exception('you can not follow yourself!')

        rel = Relationship.objects.filter(followed_by_id=profile.id, follows_id=profile_to_follow.id)
        if rel.exists():
            return JsonResponse({'status': 'exist'})
        else:
            relation = Relationship(followed_by=profile, follows=profile_to_follow)
            relation.save()
            return JsonResponse({'status': 'ok'})


class UnFollow(View):
    def post(self, request):
        profile = Profile.objects.get(user_id=request.user.profile.user_id)
        profile_to_unfollow = Profile.objects.get(id=request.POST['user_id'])
        rel = Relationship.objects.filter(followed_by_id=profile.id, follows_id=profile_to_unfollow.id)
        if rel.exists():
            rel.delete()
            return JsonResponse({'status': 'ok'})
        else:
            return JsonResponse({'status': 'error'})
