from django.urls import path, include
from django.contrib.auth import views as auth_views
from .views import SignUpView, ProfileView, MyLoginView, UpdateProfileView, Follow, UnFollow, DashboardView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('login/', MyLoginView.as_view(), name='login'),
    path('profile/<int:pk>', login_required(ProfileView.as_view()),
         name="profile"),
    path('dashboard/<int:pk>', login_required(DashboardView.as_view()), name='dashboard'),
    path('profile/edit', login_required(UpdateProfileView.as_view()), name='update_profile'),
    path('change-password/', auth_views.PasswordChangeView.as_view()),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('logout/', login_required(auth_views.LogoutView.as_view()), name='logout'),
    path('reset-password', PasswordResetView.as_view(), name='password_reset'),
    path('reset-password/done', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset-password/confirm/<uidb64>[0-9A-Za-z]+)-<token>/', PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset-password/complete/', PasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
    path('profile/follow/', Follow.as_view(), name='follow'),
    path('profile/unfollow/', UnFollow.as_view(), name='un_follow'),
]
