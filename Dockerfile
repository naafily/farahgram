FROM python:latest

WORKDIR /code

COPY requirements.txt /code/

RUN pip install -U pip
RUN pip3 install djangorestframework
RUN pip install -r requirements.txt
COPY . /code
EXPOSE 8000
CMD ['gunicorn', 'farahgram.wsgi', '8000']
