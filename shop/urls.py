from django.urls import path
from shop.views import ProductDetail, Home, AddToCart, CartDetail, RemoveProductFromCart

app_name = 'shop'

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('cart/', CartDetail.as_view(), name='cart_detail'),
    path('category/<slug:slug>/', Home.as_view(), name='category_filter'),
    path('<slug:slug>/', ProductDetail.as_view(), name='product_detail'),
    path('cart/add/<int:product_id>', AddToCart.as_view(), name='cart_add'),
    path('remove/<int:product_id>/', RemoveProductFromCart.as_view(), name='product_remove'),
]
