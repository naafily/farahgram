from django.shortcuts import get_object_or_404
from django.views import View
from django.shortcuts import redirect

from .models import Product, Category
from .cart import Cart
from django.views.generic import \
    ListView, DetailView, TemplateView
from .forms import CartAddForm


class Home(ListView):
    model = Product
    template_name = 'shop/home.html'

    def get_queryset(self):
        if "slug" in self.kwargs:
            category = get_object_or_404(Category, slug=self.kwargs['slug'])
            products = Product.objects.filter(category=category)
            return products
        else:
            return Product.objects.filter(available=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        categories = Category.objects.filter(is_sub=False)
        context['categories'] = categories
        return context


class ProductDetail(DetailView):
    model = Product
    template_name = 'shop/product_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart_form'] = CartAddForm()
        return context


class AddToCart(View):
    def post(self, request, product_id):
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        form = CartAddForm(request.POST)
        if form.is_valid():
            cart.add(product=product, quantity=form.cleaned_data['quantity'])
        return redirect('shop:cart_detail')


class CartDetail(TemplateView):
    template_name = 'shop/cart_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart'] = Cart(self.request)
        return context


class RemoveProductFromCart(View):
    def get(self, request, product_id):
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        cart.remove(product)
        return redirect('shop:cart_detail')
