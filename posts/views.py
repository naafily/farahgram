from django.views.generic import \
    ListView, DetailView, CreateView, UpdateView, DeleteView, View
from .models import Post, Like
from django.http import Http404, JsonResponse
from django.urls import reverse_lazy
from .forms import AddCommentForm
from django.contrib.messages.views import SuccessMessageMixin
from .models import Comment
from accounts.models import Profile


class PostListView(ListView):
    model = Post
    template_name = 'home.html'

    def get_queryset(self):
        profile = Profile.objects.get(user_id=self.request.user.profile.user_id)
        return Post.objects.filter(author__followed_by_set__followed_by_id=profile)


class PostDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_form = AddCommentForm()
        reply_form = AddCommentForm()
        context['comment_form'] = comment_form
        context['reply_form'] = reply_form
        is_like = False
        like = Like.objects.filter(liker=self.request.user.profile, post=self.kwargs['pk'])
        if like.exists():
            is_like = True

        context['is_like'] = is_like
        return context


class CommentCreateView(SuccessMessageMixin, CreateView):
    model = Comment
    form_class = AddCommentForm
    template_name = 'post_detail.html'
    success_message = "Your comment has been submitted successfully"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.profile = self.request.user.profile
        self.object.post = Post.objects.get(id=int(self.request.POST['post_pk']))
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'pk': int(self.request.POST['post_pk'])})


class CommentListView(ListView):
    model = Comment
    template_name = 'post_detail.html'

    def get_queryset(self):
        return Comment.objects.filter(post_id=self.kwargs['pk'])


class ReplyCreateView(SuccessMessageMixin, CreateView):
    model = Comment
    form_class = AddCommentForm
    template_name = 'post_detail.html'
    success_message = "Your reply has been submitted successfully"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.profile = self.request.user.profile
        self.object.post = Post.objects.get(id=int(self.request.POST['post_pk']))
        self.object.reply = Comment.objects.get(id=int(self.request.POST['comment_id']))
        self.object.is_reply = True
        self.object.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'pk': int(self.request.POST['post_pk'])})


class PostCreateView(CreateView):
    model = Post
    template_name = 'post_new.html'
    fields = ['date', 'caption']

    def form_valid(self, form):
        form.instance.author = self.request.user.profile
        return super().form_valid(form)


class PostEditView(UpdateView):
    model = Post
    template_name = 'post_edit.html'
    fields = ['date', 'caption']

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author != self.request.user.profile:
            raise Http404("You are not allowed to edit this Post")
        return super().dispatch(request, *args, **kwargs)


class PostDeleteView(DeleteView):
    model = Post
    template_name = 'post_delete.html'
    success_url = reverse_lazy('post')

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author != self.request.user.profile:
            raise Http404("You are not allowed to delete this Post")
        return super().dispatch(request, *args, **kwargs)


class LikeCreateView(View):
    def post(self, request):
        post = Post.objects.get(id=request.POST['post_id'])
        like = Like.objects.filter(liker=request.user.profile, post=post)
        if like.exists():
            raise Exception('you have already liked this post')
        else:
            like = Like(liker=request.user.profile, post=post)
            like.save()
            return JsonResponse({'status': 'ok'})


class UnLikeCreateView(View):
    def post(self, request):
        post = Post.objects.get(id=request.POST['post_id'])
        like = Like.objects.filter(liker=request.user.profile, post=post)
        if like.exists():
            like.delete()
            return JsonResponse({'status': 'ok'})
        else:
            raise Exception('you have already unliked this post')
