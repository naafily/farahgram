from django.contrib import admin

from .models import Post, Comment, Like


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('caption', 'get_author_username')

    def get_author_username(self, obj):
        return obj.author.user.username


admin.site.register(Comment)
admin.site.register(Like)
