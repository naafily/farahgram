from django.test import TestCase
from .models import Post
from django.urls import reverse

import datetime


class PostModelTest(TestCase):
    def setUp(self):
        Post.objects.create(caption='just a test', date=datetime.datetime.now())

    def test_text_content(self):
        post = Post.objects.get(id=1)
        expected_object_name = f'{post.caption}'
        self.assertEqual(expected_object_name, 'just a test')


class PostPageViewTest(TestCase):
    def setUp(self):
        Post.objects.create(caption='this is another test', date=datetime.datetime.now())

    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('post'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('post'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'home.html')


class PostTest(TestCase):
    def setUp(self):
        print(datetime.datetime.now())
        Post.objects.create(caption='just another test', date=datetime.datetime.now())

    def test_string_representation(self):
        post = Post(caption='just another test')
        self.assertEqual(str(post), post.caption)

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'just another test')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('post_new'), {
            'date': '2011-05-25T20:34:05.787Z',
            'caption': 'hello',
        }, ccontent_type="application/x-www-form-urlencodedc")
        self.assertEqual(response.status_code, 302)

    def test_post_update_view(self):
        response = self.client.post(reverse('post_edit', args='1'), {
            'caption': 'hellooooo mann',
        })
        self.assertEqual(response.status_code, 302)

    def test_post_delete_view(self):
        response = self.client.get(reverse('post_delete', args='1'))
        self.assertEqual(response.status_code, 302)
