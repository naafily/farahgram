from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import LikeCreateView, UnLikeCreateView, CommentCreateView, ReplyCreateView
from .views import \
    PostListView, PostDetailView, PostCreateView, PostEditView, PostDeleteView

urlpatterns = [
    path('', login_required(PostListView.as_view()), name='post'),
    path('post/new', login_required(PostCreateView.as_view()), name='post_new'),
    path('post/<int:pk>/', login_required(PostDetailView.as_view()), name='post_detail'),
    path('post/<int:pk>/edit', login_required(PostEditView.as_view()), name='post_edit'),
    path('post/<int:pk>/delete', login_required(PostDeleteView.as_view()), name='post_delete'),
    path('post/like/', login_required(LikeCreateView.as_view()), name='like'),
    path('post/unlike/', login_required(UnLikeCreateView.as_view()), name='unlike'),
    path('comment/new', login_required(CommentCreateView.as_view()), name='comment_new'),
    path('reply/new/<int:comment>/<int:reply>', login_required(ReplyCreateView.as_view()), name='reply_new'),
]
