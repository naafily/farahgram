from django.db import models
from django.urls import reverse
from accounts.models import Profile


class Post(models.Model):
    date = models.DateTimeField()
    caption = models.TextField(max_length=200)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return self.caption[:50]

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'pk': self.id})


class Comment(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    body = models.TextField(max_length=200)
    reply = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    is_reply = models.BooleanField(default=False)
    created = models.DateTimeField(null=True, auto_now_add=True)
    updated = models.DateTimeField(null=True, auto_now=True)

    class Meta:
        ordering = ('-created',)


class Like(models.Model):
    liker = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
